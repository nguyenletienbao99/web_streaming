// * Proxy
export const PROXY = 'http://localhost:3002'

// * Socket 
export const CONNECTION_PORT = 'localhost:3002/'
// * Room
export const ROOMS_ONLINE = PROXY + "/api/stream/rooms"
export const ROOM_INFO = PROXY + "/api/stream/room_info"
export const ROOM_TAGS = PROXY + "/api/stream/tags"
export const ROOM_ON_START = PROXY + "/api/stream/on_start"
// * User
export const USER_SUBSCRIBE = PROXY + "/api/user/subscribe"
// * Payment 
export const GET_ORDER_LINK = PROXY + "/api/payment/purchase_tokens"
