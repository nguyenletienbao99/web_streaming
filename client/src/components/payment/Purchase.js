import React, { useEffect, } from 'react'
import { GET_ORDER_LINK } from '../../utils/Urls'

const Purchase = ({ }) => {
    const onPurchase = async () => {
        const request = await fetch(GET_ORDER_LINK)
        const orderData = await request.json()
        window.open(orderData.orderurl || "")
    }
    useEffect(() => {
    }, [])
    return (
        <React.Fragment>
            <button onClick={onPurchase} style={{ padding: 5, }}>Purchase</button>
        </React.Fragment>
    )
}
export default Purchase