import React, { useState, useEffect, } from 'react'
import ListVideo from '../components/controls/ListVideo'
import { RoomAPI } from '../store/RoomController'
import { useDispatch, useSelector } from 'react-redux';

const Home = ({ }) => {
    const [isLoading, setIsLoading] = useState(true)
    const [videos, setVideos] = useState([])

    const dispatch = useDispatch();

    const callEffect = async () => {
        setIsLoading(true)
        try {
            const roomsOnline = await dispatch(RoomAPI.GetRoomsOnline())
            setVideos(roomsOnline?.data?.[0] || [])
        } catch (e) { console.log(e) }
        setIsLoading(false)
    }
    useEffect(() => {
        callEffect()
        // setInterval(() => {
        //     fetch('http://localhost:3002/api/stream/thumbnails')
        //         .then(data => data.json().then(videos => setVideos(videos)))
        //         .catch(err => console.log(err))
        // }
        //     , 6000
        // );
    }, [])
    return (
        <React.Fragment>
            <div style={{ width: '100%', }}>
                <ListVideo videos={videos} />
            </div>
        </React.Fragment>
    )
}
export default Home