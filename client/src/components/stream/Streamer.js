import React, { useState, useEffect } from "react";
import io from "socket.io-client";
import Video from './Video';
import { RoomAPI, OnRoomStart, } from '../../store/RoomController'
import { CONNECTION_PORT, } from '../../utils/Urls'
import Peer from 'peerjs';
import Emitter from '../controls/EventEmitter'
import { Modal } from "react-responsive-modal";
import { useDispatch, } from 'react-redux';
import makeAnimated from 'react-select/animated';
import Select from 'react-select'
import { ChatBox } from './chat/ChatBox';

const animatedComponents = makeAnimated();

let getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
let socket, local_stream

let canvas = document.createElement("canvas");
canvas.width = 320;
canvas.height = 180;

const streamerInfo = { nickname: 'nltbao', id: 1 }

function Streamer() {
    const [inputValues, set_] = useState({})
    const [visibleModal, setVisibleModal] = useState(false)
    const [tags, setTags] = useState([])
    const [_, setMutate] = useState(false)
    const dispatch = useDispatch();
    const onChangeForm = (value, stateName) => {
        inputValues[stateName] = value
        setMutate(e => !e)
    }
    const setLocalStream = (stream) => {
        let video = document.getElementById("local-video");
        video.srcObject = stream;
        video.play().then(snap)
    }
    const onStartStream = async () => {
        try {
            const { nickname, id, } = streamerInfo
            const { description, title, tags, } = inputValues
            const roomData = {
                user_id: id,
                title: title || 'title',
                description: description || 'description',
                tags: tags ? JSON.stringify(tags) : '[]',
                thumbnail: '',
            }
            const result = await OnRoomStart(roomData)
            if (result.status === 200) {
                let peer = new Peer(nickname)
                peer.on('open', (id) => {
                    console.log("Peer Connected with ID: ", id)
                    socket.emit("open_room", { nickname })
                    getUserMedia({ video: true, audio: false }, (stream) => {
                        local_stream = stream;
                        setLocalStream(local_stream)
                    }, (err) => {
                        console.log(err)
                    })
                })
                peer.on('call', (call) => {
                    console.log('calllllll')
                    call.answer(local_stream);
                })
                peer.on('connection', function (conn) {
                    console.log(conn, 'connection')
                })
            } else {
                Emitter.emit("show_toast", { message: "Error start stream!", type: "warn" })
            }
        } catch (e) { console.log(e) }
    }
    const snap = () => {
        // setInterval(() => {
        //     let video = document.getElementById("local-video");
        //     canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
        //     canvas.toBlob((blob) => {
        //         const formData = new FormData();
        //         formData.append("test", blob, "img.png")
        //         formData.append("userId", 1)
        //         fetch('http://localhost:3002/api/stream/snap', {
        //             method: 'POST',
        //             body: formData
        //         })
        //             .then(response => response.json())
        //             .then(data => console.log(data));
        //     })
        // }, 2000);
    }
    const callEffect = async () => {
        socket = io(CONNECTION_PORT);
        const listTags = await dispatch(RoomAPI.GetRoomTags())
        setTags(listTags?.data?.[0] || [])
    }
    useEffect(() => {
        callEffect()
    }, []);
    const modal = () => {
        setVisibleModal(true)
    }
    return (
        <div>
            <Modal open={visibleModal} center onClose={() => setVisibleModal(false)}>
                <div style={{ width: '40vw', height: '80vh', }}>
                    <h2>Stream info</h2>
                    <div style={{ width: '100%', marginBottom: 30, display: 'flex', flexDirection: 'row', alignItems: 'space-around', justifyContent: 'space-around', }}>
                        <div style={{ width: '30%', display: 'flex', }}>
                            <h4>Title</h4>
                        </div>
                        <div style={{ width: '70%', display: 'flex', }}>
                            <input style={{ width: '100%', }} onChange={(e) => onChangeForm(e.target.value, 'title')} placeholder="title.." />
                        </div>
                    </div>
                    <div style={{ width: '100%', marginBottom: 30, display: 'flex', flexDirection: 'row', alignItems: 'space-around', justifyContent: 'space-around', }}>
                        <div style={{ width: '30%', display: 'flex', }}>
                            <h4>Description</h4>
                        </div>
                        <div style={{ width: '70%', display: 'flex', }}>
                            <textarea style={{ width: '100%', height: 100, }} onChange={(e) => onChangeForm(e.target.value, 'description')} placeholder="Description" />
                        </div>
                    </div>
                    <div style={{ width: '100%', marginBottom: 30, display: 'flex', flexDirection: 'row', alignItems: 'space-around', justifyContent: 'space-around', }}>
                        <div style={{ width: '30%', display: 'flex', }}>
                            <h4>Tags</h4>
                        </div>
                        <div style={{ width: '70%', display: 'flex', }}>
                            <div style={{ width: '100%', }}>
                                <Select options={tags} onChange={(e) => onChangeForm(e, 'tags')}
                                    closeMenuOnSelect={false} components={animatedComponents} isMulti />
                            </div>
                        </div>
                    </div>
                    <div style={{ width: '100%', right: 0, display: 'flex', justifyContent: 'flex-end' }}>
                        <button style={{ marginRight: 10, }}>Cancel</button>
                        <button>Finish</button>
                    </div>
                </div>
            </Modal>
            <button onClick={modal}>Info</button>
            <button onClick={onStartStream}>Start</button>
            {/* <div className="meet-area"> */}
            <Video />
            {/* </div> */}
            <ChatBox roomName={'nltbao'} userChat={"nltbao"} />
        </div>
    );
}
export default Streamer;
