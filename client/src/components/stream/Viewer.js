import React, { useState, useEffect } from "react";
import io from "socket.io-client";
import { GetRoomInfo, } from '../../store/RoomController'
import { SubscribeChannel, } from '../../store/UserController'
import { CONNECTION_PORT, } from '../../utils/Urls'
import Emitter from '../controls/EventEmitter'
import Peer from 'peerjs';
import { ChatBox } from "./chat/ChatBox";

let socket;
let canvas
let getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
let local_stream;
const idUser = 3

function Viewer() {
    const [roomInfo, setRoomInfo] = useState({})
    const [isLoading, setIsLoading] = useState(true)

    const onPlayVideo = () => {
        const { nickname } = roomInfo
        let peer = new Peer()
        peer.on('open', (id) => {
            console.log("Connected with Id: " + id)
            socket.emit('user_connect_room', { roomInfo, isConnect: 1 })
            getUserMedia({ video: true, audio: false }, (stream) => {
                local_stream = stream;
                let call = peer.call(nickname, stream)
                call.on('stream', (stream) => {
                    setRemoteStream(stream);
                })
                call.on('close', () => {
                    console.log('close')
                })
            }, (err) => {
                console.log(err)
            })
        })
    }
    const setRemoteStream = (stream) => {
        let video = document.getElementById("remote-video");
        video.srcObject = stream;
        video.play();
    }
    const onSubcribe = async () => {
        const data = {
            host_id: roomInfo.id,
            follower_id: idUser,
            isSubscribed: roomInfo.isSubscribed,
        }
        const result = await SubscribeChannel(data)
        if (result.status === 200) {
            setRoomInfo({ ...roomInfo, isSubscribed: result?.data?.[0]?.[0]?.isSubscribed })
            Emitter.emit("show_toast", { message: result.msg, type: "info" })
        } else {
            Emitter.emit("show_toast", { message: result.msg, type: "error" })
        }
    }
    const callEffect = async () => {
        setIsLoading(true)
        try {
            const data = {
                roomName: window.location.pathname.slice(1,),
                idUser: idUser,
            }
            const result = await GetRoomInfo(data)
            if (result.status === 200) {
                const data = result?.data?.[0]?.[0] || {}
                data.tags = data.tags !== "" && data.tags !== undefined ? JSON.parse(data.tags) : []
                setRoomInfo(data)
            }
        } catch (e) { console.log(e) }
        setIsLoading(false)
    }
    useEffect(() => {
        // * Socket
        socket = io(CONNECTION_PORT);
        socket.on('update_viewer_watching', ({ currentViewers }) => {
            setRoomInfo({ ...roomInfo, viewers: currentViewers, })
        })
        canvas = document.createElement("canvas");
        canvas.width = 320;
        canvas.height = 180;
        callEffect()
        return () => {
            socket.emit('user_connect_room', { roomInfo, isConnect: 0 })
        }
    }, [roomInfo.nickname]);
    return (
        <div className="Client">
            <button onClick={onPlayVideo}>Play Video</button>
            <div className="meet-area">
                <video controls
                    id="remote-video"
                    autoplayfalse="true"
                    preload="auto"
                    mutedfalse="true"
                    poster="//vjs.zencdn.net/v/oceans.png">
                </video>
            </div>
            {roomInfo.nickname !== undefined && (
                <div style={{}}>
                    <div style={{ display: 'flex', justifyContent: 'space-between', width: 500, }}>
                        <div style={{ display: 'flex', }}>
                            <div style={{ marginRight: 10, }}>
                                <img src={require("../../assets/images/godbao.png")} alt="" style={{ width: 70, height: 70, borderRadius: '50%', }} />
                            </div>
                            <div style={{ display: 'flex', flexDirection: 'column', lineHeight: 0.4 }}>
                                <p style={{ fontSize: 17, fontWeight: '700', }}>{roomInfo.nickname || "nickname"}</p>
                                <p>{roomInfo.title || "title"}</p>
                                <p>{roomInfo.description || "description"}</p>
                            </div>
                        </div>
                        <p>Viewers: {roomInfo.viewers}</p>
                        <div>
                            <button style={{ background: roomInfo.isSubscribed === 1 ? 'yellow' : 'gray', }} onClick={onSubcribe}>Subscribe</button>
                        </div>
                    </div>
                    <div style={{ display: 'flex', alignItems: 'center', flexWrap: 'wrap', padding: 0 }}>
                        {Array.isArray(roomInfo?.tags) && roomInfo?.tags.map((tag, idxTag) => (
                            <span key={idxTag} className="list_video_tags">{tag.label}</span>
                        ))}
                    </div>
                </div>

            )}
            <ChatBox roomName={'nltbao'} userChat={"nmk"} />
        </div>
    );
}
export default Viewer;
