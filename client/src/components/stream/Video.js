import React from 'react'

const Video = () => {
    return (
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '100%', height: 480, background: 'brown', }}>
            <div style={{ width: '50%', }}>
                <video controls
                    id="local-video"
                    autoplayfalse="true"
                    preload="auto"
                    mutedfalse="true"
                    poster="//vjs.zencdn.net/v/oceans.png"
                    height={480}
                    width={'100%'}>
                </video>
            </div>
            <div style={{ width: '40%', height: 480, border: '1px solid black' }}>
                <div style={{ flexDirection: 'column', height: 'inherit' }}>
                    <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'flex-end', height: '8%', background: 'yellow', paddingLeft: 5, }}>
                        <div style={{ marginRight: 10, border: '1px solid black', borderBottom: '0px', marginTop: 0, borderTopRightRadius: 5, borderTopLeftRadius: 5, padding: 3, height: '80%', display: 'flex', alignItems: 'center', }}>Public</div>
                        <div style={{ marginRight: 10, border: '1px solid black', borderBottom: '0px', marginTop: 0, borderTopRightRadius: 5, borderTopLeftRadius: 5, padding: 3, height: '80%', display: 'flex', alignItems: 'center', }}>Users</div>
                        <div style={{ marginRight: 10, border: '1px solid black', borderBottom: '0px', marginTop: 0, borderTopRightRadius: 5, borderTopLeftRadius: 5, padding: 3, height: '80%', display: 'flex', alignItems: 'center', }}>Private</div>
                    </div>
                    <div style={{ height: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'space-between', }}>
                        <div style={{ height: '90%', }}>
                            <h2>All</h2>
                        </div>
                        <div style={{ background: 'red', height: '10%', }}>
                            <input style={{ width: '100%', padding: 10, }} placeholder="Comment..." />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Video