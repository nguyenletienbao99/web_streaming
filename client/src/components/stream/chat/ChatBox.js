import React, { useState, useEffect } from "react";
import queryString from 'query-string';
import io from "socket.io-client";
import { CONNECTION_PORT, } from '../../../utils/Urls'
import Emitter from '../../controls/EventEmitter'
import moment from 'moment'
// import { Input } from './Input/Input';
// import { Messages } from './Messages/Messages';
// import { TextContainer } from './TextContainer/TextContainer'

// import './Chat.css';


let socket;

export const ChatBox = ({ roomName, userChat }) => {
    const [messages, set_] = useState([]);
    const [inputValues, set__] = useState({})
    const [_, setMutate] = useState(false)

    useEffect(() => {
        socket = io(CONNECTION_PORT);
        socket.emit('join', ({ userChat, roomName }));
        socket.on('message', (payload) => {
            messages.push(payload)
            setMutate(e => !e)
        });
        socket.on('error', ({ message }) => {
            Emitter.emit("show_toast", { message, type: "error" })
        });
    }, []);
    const onChangeForm = (value, stateName) => {
        inputValues[stateName] = value
        setMutate(e => !e)
    }
    const sendMessage = () => {
        const { message } = inputValues
        socket.emit('sendMessage', { message, userChat, roomName, createdAt: moment() });
        inputValues['message'] = ''
    }

    return (
        <div className="outerContainer" style={{ border: "1px solid black" }}>

            <input
                placeholder="Type a message..."
                value={inputValues.message || ""}
                onChange={(e) => onChangeForm(e.target.value, "message")}
                onKeyUp={(e) => e.key === 'Enter' && sendMessage()}
            />
            <button onClick={sendMessage}>Cht</button>
            {messages.map((e, idx) => {
                return (
                    <div key={idx}>
                        {e.helloText !== undefined && e.helloText}
                        {e.welcomeText !== undefined && e.welcomeText}
                        {e.message !== undefined && `${e.userChat}: ${e.message} ${moment(e.createdAt).format('LT')}`}
                    </div>
                )

            })}
        </div>
    );
}