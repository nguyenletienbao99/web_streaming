import React, { useState, useEffect, } from 'react'

const Login = () => {
    const [inputValues, setInputValues] = useState({})

    const onChangeForm = (value, stateName) => {
        setInputValues({ ...inputValues, [stateName]: value })
    }
    return (
        <div>
            <h1>Login</h1>
            <input value={inputValues.username} onChange={(e) => onChangeForm(e.target.value, 'username')} placeholder="username..." />
            <input value={inputValues.password} onChange={(e) => onChangeForm(e.target.value, 'password')} placeholder="password..." />
        </div>
    )
}
export default Login