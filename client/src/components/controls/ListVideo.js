import React, { useState, useEffect, } from 'react'
import { Link } from "react-router-dom"

const ListVideo = ({ videos }) => {
    const [isLoading, setIsLoading] = useState(true)

    const callEffect = async () => {
        setIsLoading(true)
        try {
        } catch (e) { console.log(e) }
        setIsLoading(false)
    }
    useEffect(() => {
        callEffect()
    }, [])
    return (
        <React.Fragment>
            <div style={{ width: '100%', }}>
                <div className="list_video_container">
                    {videos.map((video, idx) => {
                        const tags = JSON.parse(video.tags)
                        try {
                            return (
                                <div key={idx} className="list_video_item">
                                    <div className="list_video_status">Live</div>
                                    <div className="list_video_viewers">{video.viewers || 0} viewers</div>
                                    <Link to={`/${video.nickname}`}>
                                        <img style={{ width: '100%' }} src={require("../../assets/images/default.jpg")} alt="" />
                                    </Link>
                                    <div className="list_video_description">
                                        <div style={{ padding: 5, }}>
                                            <img style={{ width: '50px', borderRadius: '50%', }} src={require('../../assets/images/footprint.png')} alt="" />
                                        </div>
                                        <div style={{ width: '80%', display: 'flex', flexDirection: 'column', padding: 10, }}>
                                            <span style={{ fontSize: 20, fontWeight: 'bold', }}>{video.title}</span>
                                            {video.description && <span style={{ fontStyle: 'italic' }}>{video.description}</span>}
                                            <span style={{ fontSize: 18, }}>{video.nickname}</span>
                                            <div style={{ display: 'flex', alignItems: 'center', flexWrap: 'wrap', padding: 0 }}>
                                                {Array.isArray(tags) && tags.map((tag, idxTag) => (
                                                    <span key={idxTag} className="list_video_tags">{tag.label}</span>
                                                ))}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        } catch (e) { }
                    })}
                </div>
            </div>
        </React.Fragment >
    )
}
export default ListVideo
