import React from 'react';

const Sidebar = ({ collapsed }) => {
    return (
        <React.Fragment>
            <div>
                <nav id="sidebar" style={{ width: collapsed ? '0px' : '250px' }}>
                    <h3>Web Stream</h3>
                </nav>
            </div>
        </React.Fragment>
    );
};

export default Sidebar;
