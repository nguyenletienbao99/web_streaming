import React, { } from 'react'
import { FaThList } from 'react-icons/fa';
import { Link } from "react-router-dom"

const Header = ({ onCollapsedChange }) => {
    return (
        <React.Fragment>
            <nav className="navbar navbar-expand-sm navbar-light bg-light">
                <FaThList onClick={onCollapsedChange} style={{ fontSize: 25, marginRight: 20, cursor: 'pointer', }} />
                <Link to="/"><div className="navbar-brand" href="#">Stream</div></Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <Link to="/streamer">
                                <div className="navbar-brand" href="#">Streamer</div>
                            </Link>
                        </li>
                        <li className="nav-item active">
                            <Link to="/purchase_tokens">
                                <div className="navbar-brand" href="#">Purchase Tokens</div>
                            </Link>
                        </li>
                    </ul>
                    <form className="form-inline my-2 my-lg-0">
                        <input className="form-control mr-sm-2" type="search" placeholder="Search..." />
                        <Link to="/login">
                            <h4 className="btn btn-outline-success my-2 my-sm-0" >Login</h4>
                        </Link>
                    </form>
                </div>
            </nav>
        </React.Fragment>
    )
}
export default Header