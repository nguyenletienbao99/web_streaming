import React, { useEffect } from "react";
import { ToastContainer, toast } from 'react-toastify';
import Emitter from './EventEmitter'

const defaultConfigs = {
    position: "top-right",
    autoClose: 3000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
}
const Toast = () => {
    useEffect(() => {
        Emitter.on('show_toast', async (data) => {
            const { message, type, configs } = data
            switch (type) {
                case "info":
                    toast.info(message, configs || defaultConfigs);
                    break;
                case "success":
                    toast.success(message, configs || defaultConfigs);
                    break;
                case "warn":
                    toast.warn(message, configs || defaultConfigs);
                    break;
                case "error":
                    toast.error(message, configs || defaultConfigs);
                    break;
                case "dark":
                    toast.dark(message, configs || defaultConfigs);
                    break;
                default:
                    toast(message, configs || defaultConfigs);
                    break;
            }
        })
    }, [])
    return (
        <ToastContainer
            position="top-right"
            autoClose={3000}
            hideProgressBar
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
        />
    )
}
export default Toast