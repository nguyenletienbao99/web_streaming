import React, { useState } from 'react'
import { BrowserRouter as Router, Switch, Route, } from "react-router-dom";
import Streamer from './components/stream/Streamer'
import Viewer from './components/stream/Viewer'
import Home from './components/Home'
import Header from './components/controls/Header'
import Sidebar from './components/controls/Sidebar'
import Login from './components/credential/Login'
import Purchase from './components/payment/Purchase'
import Toast from './components/controls/Toast'
import './App.css'
import 'react-toastify/dist/ReactToastify.css';
import "react-responsive-modal/styles.css";

function App() {
  const [collapsed, setCollapsed] = useState(true);
  const onCollapsedChange = () => {
    setCollapsed(e => !e);
  };
  return (
    <div style={{ display: 'flex', flexWrap: 'nowrap', width: '100%', height: '100%' }}>
      <Sidebar collapsed={collapsed} />
      <Toast />
      <div style={{ display: 'flex', flexDirection: 'column', width: '100%', height: '100%', }}>
        <Router>
          <Header onCollapsedChange={onCollapsedChange} />
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/login" component={Login} />
            <Route path="/purchase_tokens" component={Purchase} />
            <Route path="/streamer" component={Streamer} />
            <Route path="/*" component={Viewer} />
          </Switch>
        </Router>
      </div>
    </div>
  );
}
export default App;
