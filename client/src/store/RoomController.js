import { ROOM_TAGS, ROOMS_ONLINE, ROOM_ON_START, ROOM_INFO, } from '../utils/Urls'

const initialState = {
    roomTags: [],
    roomsOnline: [],
}
export const RoomAPI = {
    GetRoomTags: () => async (dispatch, getState) => {
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const request = await fetch(ROOM_TAGS, options)
        const roomTags = await request.json()
        dispatch({ type: 'GET_ROOM_TAGS', roomTags });
        return roomTags
    },
    GetRoomsOnline: () => async (dispatch, getState) => {
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const request = await fetch(ROOMS_ONLINE, options)
        const roomsOnline = await request.json()
        dispatch({ type: 'GET_ROOMS_ONLINE', roomsOnline });
        return roomsOnline
    },
};
export const OnRoomStart = async (roomData) => {
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(roomData)
    };
    const request = await fetch(ROOM_ON_START, options)
    const response = await request.json()
    return response
}
export const GetRoomInfo = async (data) => {
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    };
    const request = await fetch(ROOM_INFO, options)
    const response = await request.json()
    return response
}
export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_ROOM_TAGS': {
            return {
                ...state,
                roomTags: action.roomTags,
            }
        }
        case 'GET_ROOMS_ONLINE': {
            return {
                ...state,
                roomsOnline: action.roomsOnline,
            }
        }
        default:
            return state;
    }
};