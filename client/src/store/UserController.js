import { USER_SUBSCRIBE } from '../utils/Urls'
const initialState = {
    list: [],
}
export const UserAPI = {

};
export const SubscribeChannel = async (data) => {
    try {
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        };
        const request = await fetch(USER_SUBSCRIBE, options)
        const response = await request.json()
        return response
    } catch (e) {
        return e
    }
}
export const reducer = (state = initialState, action) => {
    switch (action.type) {
        default:
            return state;
    }
};
