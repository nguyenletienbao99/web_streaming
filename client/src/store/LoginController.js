const initialState = {
    list: [],
}
export const LoginAPI = {
    GetData: (data) => async (dispatch, getState) => {
        const url = "";
        const configs = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const request = await fetch(url, configs);
        const response = await request.json();
        dispatch({ type: 'GET_DATA', response });
    },
};
export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_DATA': {
            return {
                ...state,
                list: action.response,
            }
        }
        default:
            return state;
    }
};
