const express = require('express');
const router = express.Router();
const { executeQuery } = require("../database/connection")

router.get("/login", (req, res) => {
    return res.status(200).json({ msg: 'login' })
})

module.exports = router;