const express = require('express');
const router = express.Router();
const { executeQuery, payload, } = require("../database/connection")
import imgbbUploader from "imgbb-uploader";
const multer = require('multer');

router.use(express.urlencoded({
    extended: true
}))
router.use('/snap', router);

const upload = multer({
    limits: {
        fileSize: 4 * 1024 * 1024,
    }
});

router.post("/room_info", (req, res) => {
    const { roomName, idUser } = req.body
    const sql = `CALL Room_Info('${roomName}',${idUser});`
    executeQuery(sql).then(data => {
        return res.status(200).json(payload(200, 'Succeed', data))
    }).catch((err) => {
        return res.status(403).json(payload(403, err))
    })
})

router.get("/rooms", (req, res) => {
    const sql = `CALL Room_RoomsOnline();`
    executeQuery(sql).then(data => {
        return res.status(200).json(payload(200, 'Succeed', data))
    }).catch((err) => {
        return res.status(403).json(payload(403, err))
    })
})

router.get("/tags", (req, res) => {
    const sql = `CALL Room_Tags()`
    executeQuery(sql).then(data => {
        return res.status(200).json(payload(200, 'Succeed', data))
    }).catch((err) => {
        return res.status(403).json(payload(403, err))
    })
})

router.post("/on_start", (req, res) => {
    const { user_id, title, description, tags, thumbnail } = req.body
    const sql = `CALL Room_OnStart(${user_id},'${title}','${description}','${tags}','${thumbnail}');`
    executeQuery(sql).then(() => {
        return res.status(200).json(payload(200, 'Succeed'))
    }).catch((err) => {
        return res.status(403).json(payload(403, err))
    })
})

// Snap handler
router.post("/snap", upload.single('test'), (req, res) => {
    imgbbUploader({
        apiKey: '7e3177282b3a903bd1fbd8a571da24e1',
        name: "yourCustomFilename",
        expiration: 3600,
        base64string: req.file.buffer.toString('base64')
    }).then((response) => {
        const userId = req.body.userId
        const thumbnailURL = response.url
        const sql = `CALL Room_SetThumbnail(${userId},'${thumbnailURL}');`
        executeQuery(sql).then(data => {
            return res.status(200).json(data)
        }).catch((err) => {
            return res.status(403).json(err)
        })
    }).catch((error) => console.log(error));
})

router.get("/thumbnails", (req, res) => {
    const sql = `select * from rooms where status = 1`
    executeQuery(sql).then(data => {
        return res.status(200).json(data)
    }).catch((err) => {
        return res.status(403).json(err)
    })
})

module.exports = router;