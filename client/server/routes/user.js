
const express = require('express');
const router = express.Router();
const { executeQuery, payload } = require("../database/connection")

router.post("/subscribe", (req, res) => {
    const { isSubscribed, host_id, follower_id } = req.body
    const sql = `CALL User_Subscribe(${isSubscribed},${host_id},${follower_id});`
    executeQuery(sql).then(data => {
        return res.status(200).json(payload(200, 'Succeed', data))
    }).catch((err) => {
        return res.status(403).json(payload(403, err))
    })
})
module.exports = router;