const express = require('express');
const router = express.Router();
const CryptoJS = require('crypto-js');
const uuid = require('uuid');
const moment = require('moment');
const axios = require('axios');
const { executeQuery } = require("../database/connection")

router.get("/purchase_tokens", async (req, res) => {
    const orderData = await axios.post(config.endpoint, null, { params: order })
    await console.log(orderData.data)
    return res.status(200).json(orderData.data)
})

module.exports = router;

const config = {
    appid: "2554",
    key1: "sdngKKJmqEMzvh5QQcdD2A9XBSKUNaYn",
    key2: "trMrHtvjo6myautxDUiAcYsVtaeQ8nhf",
    endpoint: "https://sandbox.zalopay.com.vn/v001/tpe/createorder"
};

const embeddata = {
    merchantinfo: "embeddata123"
};

const items = [{
    itemid: "knb",
    itemname: "nguyen le tien bao",
    itemprice: 198400,
    itemquantity: 1
}];

const order = {
    appid: config.appid,
    apptransid: `${moment().format('YYMMDD')}_${uuid.v1()}`,
    appuser: "demo",
    apptime: Date.now(),
    item: JSON.stringify(items),
    embeddata: JSON.stringify(embeddata),
    amount: 50000,
    description: "ZaloPay Integration Demo",
    bankcode: "zalopayapp",
};

// appid|apptransid|appuser|amunt|apptime|embeddata|item
const data = `${config.appid}|${order.apptransid}|${order.appuser}|${order.amount}|${order.apptime}|${order.embeddata}|${order.item}`
order.mac = CryptoJS.HmacSHA256(data, config.key1).toString();
