import express from 'express'
import cors from 'cors'
const app = express()

import { initSocket } from './socket/call'

// * Middleware
app.use(cors());
app.use(express.json());

// * Routes 
app.use('/api/credential', require("./routes/credential"));
app.use('/api/stream', require("./routes/stream"));
app.use('/api/user', require("./routes/user"));
app.use('/api/payment', require("./routes/payment"));
app.use('/api/chat', require("./routes/chat"));

// * Runtime
const PORT = process.env.PORT || 3002
const server = app.listen(PORT, () => {
  console.log("Server Running on Port " + PORT);
})

// * Socket.io
initSocket(server)
