import socket from 'socket.io'
import { executeQuery } from '../database/connection'
const { addUser, removeUser, getUser, getUsersInRoom } = require('./users');

export const initSocket = (server) => {
    try {
        const io = socket(server);
        io.on("connection", (socket) => {
            // * Stream
            socket.on('open_room', function (roomInfo) {
                const { nickname, } = roomInfo
                socket.join(nickname)
            })
            socket.on('user_connect_room', (userInfo) => {
                // * isConnect: 1/0
                const { isConnect, roomInfo } = userInfo
                const sql = `CALL Room_OnUpdateViewerWatchingRoom(${isConnect},${roomInfo.roomId});`
                socket.join(roomInfo.nickname)
                executeQuery(sql).then(data => {
                    io.to(roomInfo.nickname).emit('update_viewer_watching', { currentViewers: data?.[0]?.[0]?.viewers || 0 })
                }).catch((err) => {
                    console.log(err, 'user_connect_room')
                })
            })
            // * Chat
            socket.on('join', ({ userChat, roomName }) => {
                try {
                    socket.join(roomName);
                    socket.emit('message', { roomName, helloText: `Welcome to ${roomName}` });
                    socket.broadcast.to(roomName).emit('message', { roomName, userChat, welcomeText: `${userChat} has joined room` });
                } catch (e) {
                    socket.emit('error', { message: e });
                }
            });
            socket.on('sendMessage', (payload) => {
                const { message, userChat, roomName, createdAt } = payload
                console.log(payload, 'sendMessage')
                io.to(roomName).emit('message', { userChat, message, createdAt });
            });
            socket.on("disconnect", () => {
                console.log("USER DISCONNECTED");
            });
        });
    } catch (e) { console.log(e, "initSocket") }
}