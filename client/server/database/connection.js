const mysql = require('mysql');

// * Local
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'admin',
    password: '12345678',
    database: 'web_streaming'
});

// * Hosting
// const connection = mysql.createConnection({
//     host: 'y5svr1t2r5xudqeq.cbetxkdyhwsb.us-east-1.rds.amazonaws.com',
//     user: 'xkc366o1uduop0iu',
//     password: 'kf9byjn9ixxoss2p',
//     database: 'ckvpwiv6fwgwxqxg'
// });
connection.connect();

const executeQuery = (sql) => {
    return new Promise((resolve, reject) => {
        connection.query(sql, function (error, results) {
            if (error) reject(error.sqlMessage)
            resolve(results)
        });
    })
}
const payload = (status = 200, msg = '', data = []) => {
    return { status, msg, data }
}

module.exports = { executeQuery, payload };