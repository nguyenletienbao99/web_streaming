package com.example.web_streaming.reposities;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.web_streaming.entities.RoomTag;

public interface RoomTagRepository extends JpaRepository<RoomTag, Long>{
	


}
