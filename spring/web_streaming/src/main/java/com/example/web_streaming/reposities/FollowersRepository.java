package com.example.web_streaming.reposities;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.web_streaming.entities.CommentsByVideo;
import com.example.web_streaming.entities.Followers;

public interface FollowersRepository extends JpaRepository<Followers, Long> {

}
