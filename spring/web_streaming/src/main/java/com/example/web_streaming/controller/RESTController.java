package com.example.web_streaming.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.example.web_streaming.entities.User_Accounts;
import com.example.web_streaming.reposities.User_Accounts_Reposity;

@RestController
public class RESTController {
	
	@Autowired
	User_Accounts_Reposity user_Accounts_Repository;
	
	public List<User_Accounts> test(){
		return  user_Accounts_Repository.findAll();
	}
}
