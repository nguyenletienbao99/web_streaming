package com.example.web_streaming.reposities;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.web_streaming.entities.Logs;

public interface LogsRepository extends JpaRepository<Logs, Long> {

}
