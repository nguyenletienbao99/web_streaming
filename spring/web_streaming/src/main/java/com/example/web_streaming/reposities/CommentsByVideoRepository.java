package com.example.web_streaming.reposities;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.web_streaming.entities.CommentsByVideo;

public interface CommentsByVideoRepository extends JpaRepository<CommentsByVideo, Long> {

}
