package com.example.web_streaming.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.web_streaming.entities.CommentsByVideo;
import com.example.web_streaming.entities.Followers;
import com.example.web_streaming.entities.HistoryDonate;
import com.example.web_streaming.entities.HistoryPurchase;
import com.example.web_streaming.entities.Logs;
import com.example.web_streaming.reposities.*;

@RestController
public class KhoiRESTController {

	@Autowired
	private CommentsByVideoRepository commentsByVideoRepository;
	@Autowired
	private FollowersRepository followersRepository;
	@Autowired
	private HistoryDonateRepository historyDonateRepository;
	@Autowired
	private HistoryPurchaseRepository historyPurchaseRepository;
	@Autowired
	private LogsRepository logsRepository;

	@GetMapping("/test/1")
	public List<CommentsByVideo> test(){
		return commentsByVideoRepository.findAll();	
	}
	@GetMapping("/test/2")
	public List<Followers> test2(){
		return followersRepository.findAll();	
	}
	@GetMapping("/test/3")
	public List<HistoryDonate> test3(){
		return historyDonateRepository.findAll();	
	}
	@GetMapping("/test/4")
	public List<HistoryPurchase> test4(){
		return historyPurchaseRepository.findAll();	
	}
	@GetMapping("/test/5")
	public List<Logs> test5(){
		return logsRepository.findAll();	
	}
}
