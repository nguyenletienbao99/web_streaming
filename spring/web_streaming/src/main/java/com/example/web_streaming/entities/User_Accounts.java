package com.example.web_streaming.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User_Accounts {
	@Id
	private int id;
	private String username;
	private String password;
	private String role_user;
    private boolean token;
    private String followed_channel_id;
    private String followers_id;
	private Date last_login_at;
    private Date last_changed_password_at;
    private boolean is_active;
	private boolean is_online;
    private Date created_at;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole_user() {
		return role_user;
	}
	public void setRole_user(String role_user) {
		this.role_user = role_user;
	}
	public boolean isToken() {
		return token;
	}
	public void setToken(boolean token) {
		this.token = token;
	}
	public String getFollowed_channel_id() {
		return followed_channel_id;
	}
	public void setFollowed_channel_id(String followed_channel_id) {
		this.followed_channel_id = followed_channel_id;
	}
	public String getFollowers_id() {
		return followers_id;
	}
	public void setFollowers_id(String followers_id) {
		this.followers_id = followers_id;
	}
	public Date getLast_login_at() {
		return last_login_at;
	}
	public void setLast_login_at(Date last_login_at) {
		this.last_login_at = last_login_at;
	}
	public Date getLast_changed_password_at() {
		return last_changed_password_at;
	}
	public void setLast_changed_password_at(Date last_changed_password_at) {
		this.last_changed_password_at = last_changed_password_at;
	}
	public boolean isIs_active() {
		return is_active;
	}
	public void setIs_active(boolean is_active) {
		this.is_active = is_active;
	}
	public boolean isIs_online() {
		return is_online;
	}
	public void setIs_online(boolean is_online) {
		this.is_online = is_online;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
    
    
}
