package com.example.web_streaming.reposities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.web_streaming.entities.User_Accounts;

@Repository
public interface User_Accounts_Reposity extends JpaRepository<User_Accounts, Integer> {

//	@Query(value = "select * from learning_group where is_deleted = 0", nativeQuery = true)
//	Iterable<LearningGroup> getGroupList();
//
//	@Modifying
//	@Query(value = "update learning_group set is_deleted = 0 where id = :groupId", nativeQuery = true)
//	int updateGroupStateById(int groupId);
//
//	@Modifying
//	@Query(value = """
//			update learning_group set group_Name = :#{#learningGroupDTO.getGroup_Name()}
//			, note = :#{#learningGroupDTO.getNote()}
//			, parish_id = :#{#learningGroupDTO.getParish_Id()}
//			where id = :#{#learningGroupDTO.getId()} """, nativeQuery = true)
//	int updateGroup(LearningGroupDTO learningGroupDTO);
//
//	@Query(value = "select * from learning_group where iD = :id", nativeQuery = true)
//	LearningGroupDB getGroupById(Integer id);
//
//	@Modifying
//	@Query(value = """
//			insert into learning_group values
//			(null, :#{#learningGroupDTO.getGroup_Name()}
//			, :#{#learningGroupDTO.getNote()}
//			, :#{#learningGroupDTO.isIs_Deleted()}
//			, :#{#learningGroupDTO.getParish_Id()}) """, nativeQuery = true)
//	int insertLearningGroup(LearningGroupDTO learningGroupDTO);
//
//	@Transactional
//	@Modifying
//	@Procedure("set_Delete_Learning_Group")
//	int setDeleteStateById(int id, boolean isDeleted);
//
//	@Query(value = "select * from learning_group where is_deleted = 1", nativeQuery = true)
//	Iterable<LearningGroup> getTempRemoveGroupList();

}
