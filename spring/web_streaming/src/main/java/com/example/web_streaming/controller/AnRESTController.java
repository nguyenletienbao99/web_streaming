package com.example.web_streaming.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.web_streaming.entities.RoomTag;
import com.example.web_streaming.reposities.RoomTagRepository;

@RestController
public class AnRESTController {

	@Autowired
	private RoomTagRepository roomTagRepository;

	@GetMapping("/an123")
	public List<RoomTag> test() {
		return roomTagRepository.findAll();
	}

}
