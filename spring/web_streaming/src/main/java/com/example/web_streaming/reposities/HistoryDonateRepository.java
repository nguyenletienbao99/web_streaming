package com.example.web_streaming.reposities;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.web_streaming.entities.HistoryDonate;

public interface HistoryDonateRepository extends JpaRepository<HistoryDonate, Long> {

}
