package com.example.web_streaming.reposities;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.web_streaming.entities.HistoryPurchase;

public interface HistoryPurchaseRepository extends JpaRepository<HistoryPurchase, Long> {

}
