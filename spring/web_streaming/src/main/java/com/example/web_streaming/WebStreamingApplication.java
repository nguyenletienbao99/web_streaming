package com.example.web_streaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class WebStreamingApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebStreamingApplication.class, args);
	}

}
