use web_streaming;

-- ----------------------------- ROOM -----------------------------
-- Online
DROP PROCEDURE IF EXISTS Room_RoomsOnline;
DELIMITER $$
CREATE PROCEDURE Room_RoomsOnline()
    BEGIN
		Select u.id,u.nickname, r.likes,r.title,r.description,r.tags,r.viewers,r.thumbnail, r.status from rooms r
        Left JOIN users u ON u.id=r.streamer_id
        where status<>0;
    END $$
DELIMITER ;
-- CALL Room_RoomsOnline();

-- Info
DROP PROCEDURE IF EXISTS Room_Info;
DELIMITER $$
CREATE PROCEDURE Room_Info(IN $nickname varchar(50),IN $idUser INT)
    BEGIN
		DECLARE $host_id int;
        SET $host_id=(SELECT id From Users where nickname=$nickname);
        
		SELECT u.id,r.id as roomId,u.nickname,u.fullname,u.photo,r.title,r.description,r.likes,r.viewers,IF(f.host_id IS NULL, 0, 1) as isSubscribed ,r.tags,r.created_at 
        FROM users u
        LEFT JOIN rooms r ON r.streamer_id=u.id
        LEFT JOIN followers f ON f.host_id=$host_id and f.follower_id=$idUser
        WHERE nickname=$nickname;
    END $$
DELIMITER ;
-- CALL Room_Info('nltbao',3);

-- Tags
DROP PROCEDURE IF EXISTS Room_Tags;
DELIMITER $$
CREATE PROCEDURE Room_Tags()
    BEGIN
		Select id as value,name as label from tags;
    END $$
DELIMITER ;
-- CALL Room_Tags();

-- On Start
DROP PROCEDURE IF EXISTS Room_OnStart;
DELIMITER $$
CREATE PROCEDURE Room_OnStart(IN $streamer_id INT,IN $title varchar(100),IN $description varchar(100),IN $tags LONGTEXT,IN $thumbnail varchar(100))
    BEGIN
		DECLARE $lastStatus INT; 
        SET $lastStatus=(Select status FROM rooms WHERE streamer_id=$streamer_id ORDER BY created_at DESC LIMIT 1);
		
        SELECT $lastStatus;
        IF ($lastStatus=0 OR $lastStatus IS NULL) THEN
         INSERT INTO rooms(streamer_id,title,description,tags,thumbnail) VALUES
         ($streamer_id,$title,$description,$tags,$thumbnail);
        END IF;
    END $$
DELIMITER ;
-- CALL Room_OnStart(1,'test', '[]','','');

-- On Update Viewers Watching Room
DROP PROCEDURE IF EXISTS Room_OnUpdateViewerWatchingRoom;
DELIMITER $$
CREATE PROCEDURE Room_OnUpdateViewerWatchingRoom(IN $isConnect INT,IN $roomId INT)
    BEGIN
        IF($isConnect=1) THEN
			UPDATE rooms SET viewers=viewers+1 WHERE id=$roomId;
		ELSE 
			UPDATE rooms SET viewers=viewers-1 WHERE id=$roomId;
        END IF;
        SELECT viewers FROM rooms where id=$roomId;
    END $$
DELIMITER ;
-- CALL Room_OnUpdateViewerWatchingRoom(0,6);

-- ----------------------------- USER -----------------------------
-- Subscribe
DROP PROCEDURE IF EXISTS User_Subscribe;
DELIMITER $$
CREATE PROCEDURE User_Subscribe(IN $isSubscribed INT,IN $host_id INT,IN $follower_id INT)
    BEGIN
		IF($isSubscribed=0) THEN
			IF NOT EXISTS(SELECT 1 FROM followers where host_id=$host_id AND follower_id=$follower_id) THEN
				INSERT INTO followers(host_id,follower_id) VALUES
				($host_id,$follower_id);
				SET $isSubscribed=1;
            END IF;
		ELSE 
			DELETE FROM followers WHERE host_id=$host_id and follower_id=$follower_id;	
            SET $isSubscribed=0;
		END IF;
        SELECT $isSubscribed as isSubscribed;
	END $$
DELIMITER ;									
-- CALL User_Subscribe(0,1,3);

