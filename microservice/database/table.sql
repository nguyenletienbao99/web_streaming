Create Database web_streaming;
-- Drop Database web_streaming;
use web_streaming;

Create Table accounts(
	id int AUTO_INCREMENT,
    username varchar(20) NOT NULL,
    password varchar(255) NOT NULL,
	last_login_at datetime,
	last_logout_at datetime,
    last_changed_password_at datetime,
    is_active int DEFAULT 1,
    created_at datetime DEFAULT CURRENT_TIMESTAMP,
	Primary Key(id, username)
);

Create Table users(
	id int AUTO_INCREMENT,
	nickname varchar(20) NOT NULL,
    fullname varchar(20) NOT NULL,
	role_user varchar(20) NOT NULL,
	token int DEFAULT 0,
	gender varchar(20),
	mobile varchar(20),
	photo varchar(255),
	address varchar(20),
	birthday varchar(20),
    last_changed_password_at datetime,
    created_at datetime DEFAULT CURRENT_TIMESTAMP,
	Primary Key(id,nickname)
);

Create Table followers(
	id int AUTO_INCREMENT,
	host_id int,
    follower_id int,
    created_at datetime DEFAULT CURRENT_TIMESTAMP,
	Primary Key(id)
);

Create Table rooms(
	id int AUTO_INCREMENT,
    streamer_id int,
	title varchar(50),
	description varchar(100),
    tags LONGTEXT,
	likes int DEFAULT 0,
	viewers int DEFAULT 0,
    thumbnail varchar(255),
	status int DEFAULT 1,
	created_at datetime DEFAULT CURRENT_TIMESTAMP,
    Primary Key(id)
);

Create Table comments_by_room(
	id int AUTO_INCREMENT,
    streamer_id int,
    viewer_id int,
    content varchar(255),
    created_at datetime DEFAULT CURRENT_TIMESTAMP,
    Primary Key(id)
);

Create Table room_tag(
	id int AUTO_INCREMENT,
    streamer_id int,
    tag_id int,
	created_at datetime DEFAULT CURRENT_TIMESTAMP,
    Primary Key(id)
);

Create Table tags(
	id int AUTO_INCREMENT,
    name varchar(30),
	created_at datetime DEFAULT CURRENT_TIMESTAMP,
    Primary Key(id)
);
 
Create Table history_purchase(
	id int AUTO_INCREMENT,
    sender_id int,
    receiver_id int,
    token_id int,
    amount float,
	created_at datetime DEFAULT CURRENT_TIMESTAMP,
    Primary Key(id)
);

Create Table token_currency(
	id int AUTO_INCREMENT,
    token_per_dollar int,
    is_active int,
	created_at date DEFAULT (CURRENT_DATE),
    Primary Key(id)
);

Create Table history_donate(
	id int AUTO_INCREMENT,
    sender_id int,
    receiver_id int,
    token int,
	created_at datetime DEFAULT CURRENT_TIMESTAMP,
    Primary Key(id)
);

Create Table logs(
	id int AUTO_INCREMENT,
    user_id int,
    type_name varchar(50),
    trace varchar(255),
	created_at datetime DEFAULT CURRENT_TIMESTAMP,
    Primary Key (id)
);

