use web_streaming;

-- Accounts
INSERT INTO accounts(username,password) VALUES
('bao','123'),
('mkvien','123'),
('khoi','123'),
('an','123');

INSERT INTO users(nickname,fullname,role_user,photo) VALUES
('nltbao','Nguyễn Lê Tiến Bảo','STREAMER','https://i.ibb.co/WxW63fK/Screenshot-2021-05-16-at-10-40-33.png'),
('mkvien','Maj Kĩ Ziễn','VIEWER','https://i.ibb.co/WxW63fK/Screenshot-2021-05-16-at-10-40-33.png'),
('khoi','Nguyễn Minh Khôi','VIEWER','https://i.ibb.co/WxW63fK/Screenshot-2021-05-16-at-10-40-33.png'),
('an','Nguyễn Trường An','STREAMER','https://i.ibb.co/WxW63fK/Screenshot-2021-05-16-at-10-40-33.png');

-- Rooms
-- INSERT INTO rooms(room_id, likes,description,tags, viewers,thumbnail,status) VALUES
-- (1, 23, 'Stream LOL', '', 7,'', 1),
-- (2, 12, 'Stream Game', '', 2,'', 1),
-- (3, 4, 'Stream OK', '', 4,'', 1),
-- (4, 54, 'Stream Mobile', '', 5,'', 1);

-- Tags
INSERT INTO tags(name) VALUES
('English'),
('Moba'),
('Action'),
('Horror'),
('FPS'),
('LOL'),
('Vietnam'),
('Korean'),
('FreeTalk'),
('Mobile Game'),
('Sports Game'),
('Tốc Chiến');

--------------
-- token_currency 
DELETE FROM token_currency;
INSERT INTO token_currency(token_per_dollar,is_active) VALUES (23,1);

