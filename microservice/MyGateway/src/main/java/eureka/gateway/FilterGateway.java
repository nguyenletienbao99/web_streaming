package eureka.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

import eureka.gateway.filter.SimpleFilter;

@EnableZuulProxy
@SpringBootApplication
public class FilterGateway {

  public static void main(String[] args) {
    SpringApplication.run(FilterGateway.class, args);
  }

//  @Bean
//  public SimpleFilter simpleFilter() {
//    return new SimpleFilter();
//  }

}